import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  implements OnInit{
  title = 'pages';
  langs = [ 'es', 'pt', 'en' ];
  constructor(private translateService:TranslateService){

  }

  ngOnInit(){
    let langTemp = localStorage.getItem( 'lang' );
    const browserlang = this.translateService.getBrowserLang();
    // const browserlang = 'es';
    if ( browserlang ) {
      if ( !langTemp && this.langs.indexOf( browserlang ) > -1 ) {
        this.translateService.setDefaultLang( browserlang );
        localStorage.setItem( 'lang', browserlang );
      } else {
        langTemp = langTemp ? langTemp : 'es';
        this.translateService.setDefaultLang( langTemp );
        localStorage.setItem( 'lang', langTemp );
      }
    } else {
      this.translateService.setDefaultLang( langTemp as string );
      localStorage.setItem( 'lang', langTemp as string );
    }
  }
}
